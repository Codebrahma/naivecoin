'use strict';
var CryptoJS = require("crypto-js");
var express = require("express");
var bodyParser = require('body-parser');
var WebSocket = require("ws");

var http_port = process.env.HTTP_PORT || 3001;
var p2p_port = process.env.P2P_PORT || 6001;

/**
 * Hard coded seed peers
 * https://en.bitcoin.it/wiki/Satoshi_Client_Node_Discovery#Hard_Coded_.22Seed.22_Addresses
 */
var initialPeers = process.env.PEERS ? process.env.PEERS.split(',') : [];
/**
 * Difficulty to maintain a stable mining time
 * https://en.bitcoin.it/wiki/Difficulty
 */
var difficulty = process.env.DIFFICULTY || 2;

var requiredTxnsPerBlock = process.env.TXNS_PER_BLOCK || 4;

// Reward to miner
var coinbaseReward = Number(process.env.COINBASE_REWARD) || 50;
// Flag to run the node as miner
var isMiner = Boolean(process.env.MINER);

class Block {
  /**
   * Block
   *
   * @param {number} index Number of block since Genesis block
   * @param {string} previousHash Hash of previous block in blockchain
   * @param {number} timestamp Time the block was mined
   * @param {object} data The block data (transactions)
   * @param {string} hash Hash of the block data
   * @param {string} nonce Meaningless padding to meet difficulty condition
   */
  constructor(index, previousHash, timestamp, data, hash, nonce) {
    this.index = index;
    this.previousHash = previousHash.toString();
    this.timestamp = timestamp;
    this.data = data;
    this.hash = hash.toString();
    this.nonce = nonce;
  }
}

class Transaction {
  /**
   * Transaction
   *
   * @param {string} from Sender address
   * @param {string} to Receiver address
   * @param {number} value Number of Naivecoins transferred
   * @param {number} timestamp Time the transaction was made
   * @param {string} hash Hash of the transaction data
   */
  constructor(from, to, value, timestamp, hash) {
    this.from = from;
    this.to = to;
    this.value = Number(value);
    this.timestamp = timestamp;
    this.hash = hash;
  }

  /**
   * Apply the transaction and update the records
   * @param {Transaction} txn
   */
  static Apply(txn) {
    Transaction.applied.push(txn.hash);
  }
  /**
   *
   * @param {transaction} txn
   */
  static isApplied(txn) {
    return Transaction.applied.indexOf(txn.hash) > -1;
  }
}

Transaction.list = {};    // Map of all transactions
Transaction.applied = []; // Applied transactions
Transaction.pending = []; // Pending transactions

class Account {
  /**
   * A Naivecoin acocunt
   *
   * @param {string} address Address associated with account
   * @param {number} balance Number of Naivecoins in account
   */
  constructor(address, balance) {
    this.address = address || Account.generateAddress();
    this.balance = Number(balance || 0);
    Account.list[this.address] = this;
  }

  /**
   * Get balance associated with account
   */
  getBalance() {
    return this.balance;
  }

  /**
   * Increment balance by amount
   *
   * @param {number} amount
   */
  incBalance(amount) {
    this.balance += Number(amount);
  }

  /**
   * Decrement balance by amount
   * @param {number} amount
   */
  decBalance(amount) {
    this.balance -= Number(amount);
  }

  /**
   * Generate address for account
   */
  static generateAddress() {
    return CryptoJS.SHA256(String(process.pid) + Date.now()).toString();
  }

  /**
   * Determines if the address in question
   * is associated with this account
   * @param {string} address
   */
  static has(address) {
    return Boolean(Account.list[address]);
  }

  /**
   * Get the address if it is associated
   * with this account
   * @param {string} address
   */
  static get(address) {
    return Account.list[address];
  }
}

Account.list = {};

// Array to store the Block Chain
var blockchain = [];
// Array to store sockets of peers
var sockets = [];
// Message types allowed in Naive protocol
var MessageType = {
  QUERY_LATEST: 0,
  QUERY_ALL: 1,
  RESPONSE_BLOCKCHAIN: 2,
  NEW_TXNS: 3,
  QUERY_TXN: 4,
  QUERY_ALL_TXNS: 5,
  RESPONSE_TXNS: 6
};

/**
 * Generates hard-coded genesis block
 * for the blockchain
 * https://en.bitcoin.it/wiki/Genesis_block
 */
var generateGenesisBlock = () => {
  var txnData = {
    from: null,
    to: 'e23090a61fabced0972658ae07493e3e3e12568a3dd9e99d9da471e7bdcf90da',
    value: 50,
    timestamp: 1465154705
  };
  txnData.hash = calculateHashForTxn(txnData);  // Calculate hash of the transaction
  // Generate genesis transaction
  var genesisTransaction = new Transaction(txnData.from, txnData.to, txnData.value, txnData.timestamp, txnData.hash);
  // Store it in list of seen transactions
  Transaction.list[genesisTransaction.hash] = genesisTransaction;
  // Apply the transaction
  Transaction.Apply(genesisTransaction);
  // Update local copy of affected accounts by this transaction
  updateAccount(genesisTransaction);
  // Genesis block
  var blockData = {
    index: 0,
    previousHash: '0000000000000000000000000000000000000000000000000000000000000000',
    timestamp: 1465154705,
    data: {
      txns: [ genesisTransaction.hash ]
    },
    nonce: 0
  };
  // Generate hash for genesis block
  blockData.hash = calculateHashForBlock(blockData);
  // Generate genesis block
  var genesisBlock = new Block(blockData.index, blockData.previousHash, blockData.timestamp, blockData.data, blockData.hash, blockData.nonce);
  // Initiate the blockchain with the genesis block
  blockchain = [ genesisBlock ];
};

/**
 * Gets the genesis block form local copy of blockchain
 */
var getGenesisBlock = () => {
  return blockchain[0];
};

/**
 * Mine the block
 * @param {Block} block
 */
var mineBlock = (block) => {
  var newBlock = generateNextBlock(block);
  addBlock(newBlock);
  broadcast(responseLatestMsg());
  console.log('block added: ' + JSON.stringify(newBlock));
};

/**
 * Return pending transactionsto be
 * included in the block to be mined
 */
var generateBlockData = () => {
  return { txns: Transaction.pending };
};

/**
 * Mark the transaction as seen by the node
 * @param {Transaction} txn
 */
var saveTransaction = (txn) => {
  Transaction.list[txn.hash] = txn;
};

/**
 * Mark the transaction to be added to a block
 * @param {Transaction} txn
 */
var stageTransaction = (txn) => {
  Transaction.pending.push(txn.hash);
};

/**
 * Determines if ther are enough pending
 * transactions to be added to a block
 */
var readyToMineBlock = () => {
  return Transaction.pending.length >= requiredTxnsPerBlock;
};

/**
 * Broadcast a transaction to all peers
 * by its hash
 * @param {string} txnHash
 */
var broadcastTxn = (txnHash) => {
  broadcast({ 'type': MessageType.NEW_TXNS, 'data': JSON.stringify({ txnHash: Transaction.list[txnHash] }) });
};

/**
 * Generates the coinbase transaction to be
 * added to the end of the block to reward
 * the miner for mining the block
 */
var generateCoinbaseTxn = () => {
  var txnData = {
    from: null,
    to: myAccount.address,
    value: coinbaseReward,
    timestamp: Date.now()
  };
  txnData.hash = calculateHashForTxn(txnData);
  var coinbaseTxn = new Transaction(txnData.from, txnData.to, txnData.value, txnData.timestamp, txnData.hash);
  Transaction.list[coinbaseTxn.hash] = coinbaseTxn;
  updateAccount(coinbaseTxn);
  Transaction.Apply(coinbaseTxn);
  return coinbaseTxn;
};

/**
 * Generate block data from epnding transactions
 */
var generateBlockData = () => {
  var stagedTransactions = Transaction.pending.slice(0, requiredTxnsPerBlock);
  Transaction.pending = Transaction.pending.slice(requiredTxnsPerBlock);
  stagedTransactions.push(generateCoinbaseTxn().hash);
  return { txns: stagedTransactions };
};

/**
 * Generate hash of a transaction
 * @param {Transaction} txn
 */
var calculateHashForTxn = (txn) => {
  return calculateHash(txn.from, txn.to, txn.value, txn.timestamp, '');
};

/**
 * Remove a transaction from list of pending
 * transactions. posibly because it's already
 * included in a new block
 * @param {string} txnHash
 */
var unstageTransaction = (txnHash) => {
  var index = Transaction.pending.indexOf(txnHash);
  if (index > -1) {
    Transaction.pending.splice(index);
  }
  console.log('transaction already mined. unstaging: ' + txnHash);
};

/**
 * Add transaction to seen list and broadcast.
 * If not already mined, add to list of pending txns
 * @param {Transaction} txn
 * @param {boolean} alreadyMined
 */
var addTransaction = (txn, alreadyMined) => {
  txn.timestamp = txn.timestamp || Date.now();
  txn.hash = calculateHashForTxn(txn);
  var transaction = new Transaction(txn.from, txn.to, txn.value, txn.timestamp, txn.hash);
  // Mark it seen if not already marked
  if (!Transaction.list[transaction.hash]) {
    // Mark to be mined if not already mined
    if (!alreadyMined) {
      // Check if transfer is valid acc. to local copy of accounts record
      if(isTransferValid(txn)) {
        // If running as miner node, save transaction to be mined
        if (isMiner) {
          saveTransaction(transaction);
          console.log('transaction added: ' + JSON.stringify(transaction));
          stageTransaction(transaction);
          // If addition of this transaciton fulfills the
          // block mining threshold, start mining the block
          if (readyToMineBlock()) {
            console.log('enough transactions received');
            mineBlock(generateBlockData());
          } else {
            console.log(requiredTxnsPerBlock - Transaction.pending.length + ' more transactions required for mining');
          }
        } else {
          }
      }
    } else {
      // if mined, just save and broadcast
      saveTransaction(transaction);
      broadcastTxn(transaction.hash);
      console.log('transaction added: ' + JSON.stringify(transaction));
    }
  } else {
    console.log('duplicate transaction');
    if (alreadyMined) {
      unstageTransaction(txn.hash);
    }
  }
  // Apply the transaction and update local records
  // of accounts affected
  if (!Transaction.isApplied(txn)) {
    updateAccount(txn);
    Transaction.Apply(txn);
  }
};

/**
 * Calculats hash of the block and returns it
 * @param {Block} block
 */
var calculateHashForBlock = (block) => {
  return calculateHash(block.index, block.previousHash, block.timestamp, block.data, block.nonce);
};

/**
 * Return hash of the arguments concatenated
 * @param {number} index
 * @param {string} previousHash
 * @param {number} timestamp
 * @param {object} data
 * @param {string} nonce
 */
var calculateHash = (index, previousHash, timestamp, data, nonce) => {
  return CryptoJS.SHA256(index + previousHash + timestamp + data + nonce).toString();
};

/**
 * Determines if the transfer represented by the
 * given transaction is valid and allowed
 * @param {Transaction} txn
 */
var isTransferValid = (txn) => {
  var sender = Account.get(txn.from);
  return sender && sender.balance >= txn.value;
};

/**
 * Updates the balance of accounts
 * affected by the given transaction
 * @param {Transaction} txn
 */
var updateAccount = (txn) => {
  var sender = Account.get(txn.from);
  var receiver = Account.get(txn.to) || new Account(txn.to);
  // If normal transaction
  if (sender) {
    if (isTransferValid(txn)) {
      // Debit balance from sender, and credit to receiver
      sender.decBalance(txn.value);
      receiver.incBalance(txn.value);
    } else {
      console.error('Sender doesn\'t have enough CbCoins');
    }
  } else {
    // For coinbase transactions, there won't be any debit
    receiver.incBalance(txn.value);
  }
};

// Initialize the account associated with this node
var myAccount = new Account(process.env.ADDRESS, process.env.BALANCE);
// Generate genesis block
generateGenesisBlock();

/**
 * Initializes HTTP server which provides
 * info about current state of the node
 */
var initHttpServer = () => {
  var app = express();
  app.use(bodyParser.json());

  app.get('/blocks', (req, res) => res.send(JSON.stringify(blockchain)));
  app.get('/block/:id', (req, res) => res.send(JSON.stringify(blockchain.find((block) => block.hash === req.params.id))));
  app.get('/transactions', (req, res) => res.send(JSON.stringify(Transaction.list)));
  app.get('/transaction/:id', (req, res) => res.send(JSON.stringify(Transaction.list[req.params.id])));
  app.get('/accounts', (req, res) => res.send(Account.list));
  app.get('/account/:id', (req, res) => res.send(Account.get(req.params.id)));
  app.get('/my-account', (req, res) => res.send(myAccount));
  app.post('/transact', (req, res) => {
    var txnData = req.body.data;
    addTransaction(txnData);
    res.send();
  });
  app.get('/peers', (req, res) => {
    res.send(sockets.map(s => s._socket.remoteAddress + ':' + s._socket.remotePort));
  });
  app.post('/addPeer', (req, res) => {
    connectToPeers([req.body.peer]);
    res.send();
  });

  /**
   * Route to activate malicious code in the node
   * NOTE: STRICTLY FOR TESTING
   */
  app.post('/turnMalicious', (req, res) => {
    console.log('node turning malicious...');
    var targetedBlock = blockchain[req.body.index];
    // Replace a block's data
    targetedBlock.data = req.body.data;
    blockchain[targetedBlock.index].data = targetedBlock.data;
    var prevBlock = blockchain[targetedBlock.index -1];
    // Generate the blockchain again
    for (var i = targetedBlock.index; i < blockchain.length; i++) {
      var block = blockchain[i];
      var pow = generatePoW(block.index, prevBlock.hash, block.timestamp, block.data);
      block = new Block(block.index, prevBlock.hash, block.timestamp, block.data, pow.hash, pow.nonce);
      blockchain[i] = block;
      prevBlock = block;
    }
    console.log('local blockchain corrupted...');
    // Broadcast the corrupted blockchain
    broadcast({ 'type': MessageType.RESPONSE_BLOCKCHAIN, 'data': JSON.stringify(blockchain) });
    console.log('corrrupted blockchain broadcasted');
    res.send();
  });
  app.listen(http_port, () => console.log('Listening http on port: ' + http_port));
};

/**
 * Initializes WebSockets server for P2P communication
 * and messaging between Naivecoin nodes
 */
var initP2PServer = () => {
  var server = new WebSocket.Server({port: p2p_port});
  server.on('connection', ws => initConnection(ws));
  console.log('listening websocket p2p port on: ' + p2p_port);

};

/**
 * Initialize connection with a peer
 * @param {Websocket} ws
 */
var initConnection = (ws) => {
  sockets.push(ws);
  initMessageHandler(ws);
  initErrorHandler(ws);
  write(ws, queryChainLengthMsg());
};

/**
 *
 * Handle messages from a peer
 * @param {Websocket} ws
 */
var initMessageHandler = (ws) => {
  ws.on('message', (data) => {
    var message = JSON.parse(data);
    console.log('Received message' + JSON.stringify(message));
    switch (message.type) {
      case MessageType.QUERY_LATEST:
      write(ws, responseLatestMsg());
      break;
      case MessageType.QUERY_ALL:
      write(ws, responseChainMsg());
      break;
      case MessageType.RESPONSE_BLOCKCHAIN:
      handleBlockchainResponse(message);
      break;
      case MessageType.NEW_TXNS:
      handleNewTxns(message);
      break;
      case MessageType.QUERY_TXN:
      handleTxnsQuery(ws, message);
      break;
      case MessageType.QUERY_ALL_TXNS:
      handleTxnsQuery(ws);
      break;
      case MessageType.RESPONSE_TXNS:
      handleNewTxns(message, true);
      break;
    }
  });
};

/**
 * Handle connection disruption
 * with a peer
 * @param {Websocket} ws
 */
var initErrorHandler = (ws) => {
  var closeConnection = (ws) => {
    console.log('connection failed to peer: ' + ws.url);
    sockets.splice(sockets.indexOf(ws), 1);
  };
  ws.on('close', () => closeConnection(ws));
  ws.on('error', () => closeConnection(ws));
};

/**
 * Determines if the Proof of Work is valid
 * https://en.bitcoin.it/wiki/Proof_of_work
 *
 * @param {string} pow
 */
var isValidPoW = (pow) => {
  return (new RegExp('^[0]{' + difficulty + '}')).test(pow);
};

/**
 * Generate Proof of work
 * https://en.bitcoin.it/wiki/Proof_of_work
 *
 * @param {number} index
 * @param {string} previousHash
 * @param {number} nextTimestamp
 * @param {object} blockData
 */
var generatePoW = (index, previousHash, nextTimestamp, blockData) => {
  var hash;
  var nonce = -1;
  do {
    hash = calculateHash(index, previousHash, nextTimestamp, blockData, ++nonce);
  }
  while (!isValidPoW(hash));

  return {
    hash: hash,
    nonce: nonce
  };
};

/**
 * Generate a new block from block data
 * @param {object} blockData
 */
var generateNextBlock = (blockData) => {
  var previousBlock = getLatestBlock();
  var nextIndex = previousBlock.index + 1;
  var nextTimestamp = new Date().getTime() / 1000;
  var pow = generatePoW(nextIndex, previousBlock.hash, nextTimestamp, blockData);
  return new Block(nextIndex, previousBlock.hash, nextTimestamp, blockData, pow.hash, pow.nonce);
};

/**
 * Add a new block to blockchain
 * @param {Block} newBlock
 */
var addBlock = (newBlock) => {
  if (isValidNewBlock(newBlock, getLatestBlock())) {
    blockchain.push(newBlock);
  }
};

/**
 * determines if newBlock is valid
 * successor of previousBlock
 * @param {Block} newBlock
 * @param {Block} previousBlock
 */
var isValidNewBlock = (newBlock, previousBlock) => {
  if (previousBlock.index + 1 !== newBlock.index) {
    console.log('invalid index');
    return false;
  } else if (previousBlock.hash !== newBlock.previousHash) {
    console.log('invalid previous hash');
    return false;
  } else if (calculateHashForBlock(newBlock) !== newBlock.hash) {
    console.log(typeof (newBlock.hash) + ' ' + typeof calculateHashForBlock(newBlock));
    console.log('invalid hash: ' + calculateHashForBlock(newBlock) + ' ' + newBlock.hash);
    return false;
  }
  return true;
};

/**
 * Connect to list of peers
 * @param {array} newPeers
 */
var connectToPeers = (newPeers) => {
  newPeers.forEach((peer) => {
    var ws = new WebSocket(peer);
    ws.on('open', () => initConnection(ws));
    ws.on('error', () => {
      console.log('connection failed')
    });
  });
};

/**
 * Handle new blockchain message from peers
 * @param {object} message
 */
var handleBlockchainResponse = (message) => {
  var receivedBlocks = JSON.parse(message.data).sort((b1, b2) => (b1.index > b2.index));
  var latestBlockReceived = receivedBlocks[receivedBlocks.length - 1];
  var latestBlockHeld = getLatestBlock();
  if (latestBlockReceived.index > latestBlockHeld.index) {
    console.log('blockchain possibly behind. We got: ' + latestBlockHeld.index + ' Peer got: ' + latestBlockReceived.index);
    if (latestBlockHeld.hash === latestBlockReceived.previousHash) {
      console.log("We can append the received block to our chain");
      blockchain.push(latestBlockReceived);
      broadcast(responseLatestMsg());
      processEachTxn(latestBlockReceived);
    } else if (receivedBlocks.length === 1) {
      console.log("We have to query the chain from our peer");
      broadcast(queryAllMsg());
    } else {
      console.log("Received blockchain is longer than current blockchain");
      replaceChain(receivedBlocks);
      broadcast(queryAllTxns());
    }
  } else {
    console.log('received blockchain is not longer than received blockchain. Do nothing');
  }
};

/**
 * Handle new transactions
 *
 * @param {object} message
 * @param {boolean} alreadyMined
 */
var handleNewTxns = (message, alreadyMined) => {
  var newTxns = JSON.parse(message.data);
  for (var hash in newTxns) {
    addTransaction(newTxns[hash], alreadyMined);
  }
}

/**
 * Replace local copy of blockchain
 * with newBlocks
 * @param {[Block]} newBlocks
 */
var replaceChain = (newBlocks) => {
  if (isValidChain(newBlocks) && newBlocks.length > blockchain.length) {
    console.log('Received blockchain is valid. Replacing current blockchain with received blockchain');
    blockchain = newBlocks;
    broadcast(responseLatestMsg());
  } else {
    console.log('Received blockchain invalid');
  }
};

/**
 * Validates the feasibilty of the given blockchain
 * @param {[Block]} blockchainToValidate
 */
var isValidChain = (blockchainToValidate) => {
  if (JSON.stringify(blockchainToValidate[0]) !== JSON.stringify(getGenesisBlock())) {
    return false;
  }
  var tempBlocks = [blockchainToValidate[0]];
  for (var i = 1; i < blockchainToValidate.length; i++) {
    if (isValidNewBlock(blockchainToValidate[i], tempBlocks[i - 1])) {
      tempBlocks.push(blockchainToValidate[i]);
    } else {
      return false;
    }
  }
  return true;
};

var getLatestBlock = () => blockchain[blockchain.length - 1];
var queryChainLengthMsg = () => ({'type': MessageType.QUERY_LATEST});
var queryAllMsg = () => ({'type': MessageType.QUERY_ALL});
var queryAllTxns = () => ({'type': MessageType.QUERY_ALL_TXNS});
var queryTxn = (hash) => ({'type': MessageType.QUERY_TXN, data: hash});
var responseChainMsg = () =>({
  'type': MessageType.RESPONSE_BLOCKCHAIN, 'data': JSON.stringify(blockchain)
});
var responseLatestMsg = () => ({
  'type': MessageType.RESPONSE_BLOCKCHAIN,
  'data': JSON.stringify([getLatestBlock()])
});

var processEachTxn = (block) => {
  var txns = block.data.txns;
  txns.forEach((hash) => {
    if (!Transaction.list[hash]) {
      console.log('transaction missing: ' + hash);
      broadcast(queryTxn(hash));
    } else {
      unstageTransaction(hash);
    }
  });
};

var handleTxnsQuery = (ws, msg) => {
  if (msg && msg.data) {
    if (Transaction.list[msg.data]) {
      write(ws, {
        type: MessageType.RESPONSE_TXNS,
        data: JSON.stringify({ hash: Transaction.list[msg.data] })
      });
    }
  } else {
    write(ws, {
      type: MessageType.RESPONSE_TXNS,
      data: JSON.stringify(Transaction.list)
    });
  }
};

var write = (ws, message) => ws.send(JSON.stringify(message));
var broadcast = (message) => sockets.forEach(socket => write(socket, message));

connectToPeers(initialPeers);
initHttpServer();
initP2PServer();
